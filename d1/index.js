console.log('Hi, B248');
console.log(document); //result - html document
console.log(document.querySelector('#txt-first-name'));
/*
	document - refers to the whole web page
	querySelector - used to select a specified element (obj) as long as it is inside the html tag (HTML ELEMENT)
	-takes a string input that is formatted like css selector
	-can select elements regardless if the string is an id, class or a tag as long as the element is existing in the webpage
*/
/*
	Alternative methods that we use aside from querySelector in retrieving elements

	document.getElementById();
	document.getELementsByClassName();
	document.getElementsByTagName();
*/

const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

txtFirstName.addEventListener('keyup', (e)=>{
	spanFullName.innerHTML = txtFirstName.value
});

txtLastName.addEventListener('keyup', (e)=>{
	spanFullName.innerHTML = txtLastName.value
});

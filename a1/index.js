const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

function handleFullName(event){
	spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
};

txtFirstName.addEventListener('keyup', handleFullName);
txtLastName.addEventListener('keyup', handleFullName);